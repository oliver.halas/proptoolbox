%test_isentropic
clc ;
clear;
addpath Modules


M = logspace(-1, 1);
gamma = 1.2;


B = 1./arrayfun(@(z)Isentropic.stag_temperature_ratio(z,gamma),M); 
C = 1./arrayfun(@(z)Isentropic.stag_pressure_ratio(z,gamma),M);
D = 1./arrayfun(@(z)Isentropic.stag_density_ratio(z,gamma),M);

loglog(M,B)
hold on
loglog(M,C)
hold on
loglog(M,D)
hold on
xline(1,'r');
legend('T/T_0','p / p_0','\rho/ \rho_0')
hold on 


xlabel('Mach number [-]');
ylabel('Static / stagnation [-]');
title("Isentropic flow relations for K = " + gamma );


xlim([0.1 10]);
ylim([1e-3, 1.1]);

grid on;

hold off

% Declare engine design parameters
p_c = 10e6;          % Chamber pressure [units: pascal]
p_e = 100e3;         % Exit pressure [units: pascal]
m_molar = 20e-3;     % Exhaust molar mass [units: kilogram mole**1]
T_c = 3000;             % Chamber temperature [units: kelvin]
v_1=0;

v_e = Isentropic.velocity( v_1, p_c, T_c, p_e, gamma, m_molar);

fprintf('Exit velocity %12.3f m/s ',v_e );
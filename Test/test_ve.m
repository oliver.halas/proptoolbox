addpath Modules

M_1 = linspace(0.1, 10);         % Mach number [units: dimensionless]
gamma = 1.2;                     % Exhaust heat capacity ratio [units: dimensionless]

A = arrayfun(@(z)Nozzle.area_from_mach(z,gamma),M_1);

% semilogx(M_1,A);
loglog(M_1,A)

xlabel('Mach number M_1 [-]');
ylabel('Area ratio A_1 / A_2 [-]');
title('Mach-Area relation for M_2 = 1');
ylim([1, 1e3]);
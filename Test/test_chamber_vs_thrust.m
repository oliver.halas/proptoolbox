clc;
clear;

addpath Modules

p_c = linspace(1e6, 20e6);       % Chamber pressure [units: pascal]
p_e = 100e3;                     % Exit pressure [units: pascal]
p_a = 100e3;                     % Ambient pressure [units: pascal]
gamma = 1.2;                     % Exhaust heat capacity ratio [units: dimensionless]
A_t = pi * (0.1 / 2)^2;          % Throat area [units: meter**2]


% Compute thrust [units: newton]

F = arrayfun(@(z)Nozzle.thrust(A_t, z, p_e, gamma,'',''),p_c);
figure(1)
plot( (p_c * 1e-6), (F * 1e-3));

xlabel('Chamber pressure p_c [MPa]');
ylabel('Thrust F [kN]');
title("Thrust vs chamber pressure at p_e = p_a = " + ( p_e * 1e-3) + " kPa" );

% p_c=7e6;
% p_e=80e3;
% er = linspace(1, 2000);
% C_F = arrayfun(@(z)Nozzle.thrust_coef(p_c,p_e,gamma,p_a,z),er);
% figure(2)
% plot(er,C_F);

% 
% Nozzle.thrust_coef(p_c,p_e,gamma,p_a,1)
% Nozzle.thrust_coef(p_c,p_e,gamma,p_a,10)


classdef Nozzle
    %NOZZLE Summary of this class goes here
    %   Detailed explanation goes here
    
    methods (Static=true)
        
        function a_from_m = area_from_mach(M, gamma)

%         """Find the area ratio for a given Mach number.
%         For isentropic nozzle flow, a station where the Mach number is :math:`M` will have an area
%         :math:`A`. This function returns that area, normalized by the area of the nozzle throat
%         :math:`A_t`.
%         See :ref:`mach-area-tutorial-label` for a physical description of the Mach-Area relation.
%         Reference: Rocket Propulsion Elements, 8th Edition, Equation 3-14.
%         Arguments:
%             M (scalar): Mach number [units: dimensionless].
%             gamma (scalar): Ratio of specific heats [units: dimensionless].
%         Returns:
%             scalar: Area ratio :math:`A / A_t`.
%         """
     
           a_from_m = 1 / M * (2 / (gamma + 1) * (1 + (gamma - 1) / 2 * M^2)) ^((gamma + 1) / (2 * (gamma - 1)));
        end
         
        function choked = is_choked(p_c,p_e,gamma)
%                 Determine whether the nozzle flow is choked.

%                 See :ref:`choked-flow-tutorial-label` for details.
%                 Reference: Rocket Propulsion Elements, 8th Edition, Equation 3-20.
%                 Arguments:
%                     p_c (scalar): Nozzle stagnation chamber pressure [units: pascal].
%                     p_e (scalar): Nozzle exit static pressure [units: pascal].
%                     gamma (scalar): Exhaust gas ratio of specific heats [units: dimensionless].
%                 Returns:
%                     bool: True if flow is choked, false otherwise.
          
            choked = p_e/p_c < (2 / (gamma + 1))^(gamma / (gamma - 1));
        end
        
        function m_dot = mass_flow(A_t, p_c, T_c, gamma, m_molar)
%             Find the mass flow through a choked nozzle.
%             Given gas stagnation conditions and a throat area, find the mass flow through a
%             choked nozzle. See :ref:`choked-flow-tutorial-label` for details.
%             Reference: Rocket Propulsion Elements, 8th Edition, Equation 3-24.
%             Arguments:
%                 A_t (scalar): Nozzle throat area [units: meter**2].
%                 p_c (scalar): Nozzle stagnation chamber pressure [units: pascal].
%                 T_c (scalar): Nozzle stagnation temperature [units: kelvin].
%                 gamma (scalar): Exhaust gas ratio of specific heats [units: dimensionless].
%                 m_molar (scalar): Exhaust gas mean molar mass [units: kilogram mole**-1].
%             Returns:
%                 scalar: Mass flow rate :math:`\dot{m}` [units: kilogram second**-1].
       
                m_dot=(A_t * p_c * gamma / (gamma * constants.R_univ / m_molar * T_c)^0.5 * (2 / (gamma + 1))^((gamma + 1) / (2 * (gamma - 1))));
        end
        
        function F = thrust(A_t, p_c, p_e,gamma,p_a,er)
%         Nozzle thrust force.
%         Arguments:
%             A_t (scalar): Nozzle throat area [units: meter**2].
%             p_c (scalar): Nozzle stagnation chamber pressure [units: pascal].
%             p_e (scalar): Nozzle exit static pressure [units: pascal].
%             gamma (scalar): Exhaust gas ratio of specific heats [units: dimensionless].
%             p_a (scalar, optional): Ambient pressure [units: pascal]. If None,
%                 then p_a = p_e.
%             er (scalar, optional): Nozzle area expansion ratio [units: dimensionless]. If None,
%                 then p_a = p_e.
%         Returns:
%             scalar: Thrust force [units: newton].

            if( (isempty(er)&& ~isempty(p_a)) || (isempty(p_a)&& ~isempty(er)))
                fprintf ('Both p_a and er must be provided.');
            end
            
            if ~isempty(p_a) && ~isempty(er)
                p_a='';
                er='';
            end
            
   
                 F = A_t * p_c * Nozzle.thrust_coef(p_c,p_e,gamma,p_a,er);
                 
        end
         
        function C_F = thrust_coef(p_c,p_e,gamma,p_a,er)
%                 Nozzle thrust coefficient, :math:`C_F`.
%                 The thrust coefficient is a figure of merit for the nozzle expansion process.
%                 See :ref:`thrust-coefficient-label` for a description of the physical meaning of the
%                 thrust coefficient.
%                 Reference: Equation 1-33a in Huzel and Huang.
%                 Arguments:
%                     p_c (scalar): Nozzle stagnation chamber pressure [units: pascal].
%                     p_e (scalar): Nozzle exit static pressure [units: pascal].
%                     gamma (scalar): Exhaust gas ratio of specific heats [units: dimensionless].
%                     p_a (scalar, optional): Ambient pressure [units: pascal]. If None,
%                         then p_a = p_e.
%                     er (scalar, optional): Nozzle area expansion ratio [units: dimensionless]. If None,
%                         then p_a = p_e.
%                 Returns:
%                     scalar: The thrust coefficient, :math:`C_F` [units: dimensionless].
   
            if( (isempty(er)&& ~isempty(p_a)) || (isempty(p_a)&& ~isempty(er)))
                fprintf ('Both p_a and er must be provided.');
            end
            C_F = (2 * gamma^2 / (gamma - 1) * (2 / (gamma + 1))^((gamma + 1) / (gamma - 1)) * (1 - (p_e / p_c)^((gamma - 1) / gamma)))^0.5;

            if ~isempty(p_a) && ~isempty(er)
                
                C_F = C_F + ((p_e - p_a) / p_c)*er;
                
            end
   
            
        end
           
    end
end

